# README #

### What is this repository for? ###

Coin dispense server

### How do I get set up? ###
Make sure you have Java 8 and Maven 
Run mvn clean install
Run jar file
REST server runs on port 9045
Swagger UI link: http://127.0.0.1:9045/swagger-ui.html
Socket server will run on port 8909
To run as socket server, uncomment @PostConstruct in class SocketServer
Login with credentials scott/tiger
Password has to be MD5 hashed e.g scott/43B90920409618F188BFC6923F16B9FA
