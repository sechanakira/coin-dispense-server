package com.shingirai.coindispense;

import com.shingirai.coindispense.logic.BreakdownCalculator;
import com.shingirai.coindispense.model.CurrencyBreakdownRequest;
import com.shingirai.coindispense.model.CurrencyBreakdownResponse;
import com.shingirai.coindispense.model.RandCurrency;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

import java.util.stream.Collectors;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CoinDispenseApplicationTests {

	@Autowired
	private BreakdownCalculator breakdownCalculator;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testBreakdown(){
		CurrencyBreakdownRequest currencyBreakdownRequest = new CurrencyBreakdownRequest();
		currencyBreakdownRequest.setAmount(255.55);
		CurrencyBreakdownResponse currencyBreakdownResponse = breakdownCalculator.breakDownCurrency(currencyBreakdownRequest);
		assertNotNull(currencyBreakdownResponse);
		long totalDenominations = currencyBreakdownResponse.getCurrencyList().stream()
				.collect(Collectors.counting());
		assertEquals(6,totalDenominations);
		long numHundreds = currencyBreakdownResponse.getCurrencyList().stream()
				.filter(denomination->denomination.equals(RandCurrency.ONE_HUNDRED))
				.collect(Collectors.counting());
		assertEquals(2,numHundreds);
	}


}
