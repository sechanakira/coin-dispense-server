package com.shingirai.coindispense.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class AuthenticationResponse implements Serializable {

    private String token;
    private AuthStatus authStatus;
}
