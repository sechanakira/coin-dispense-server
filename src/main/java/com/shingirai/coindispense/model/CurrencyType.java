package com.shingirai.coindispense.model;

public enum CurrencyType {

    NOTE,
    COIN

}
