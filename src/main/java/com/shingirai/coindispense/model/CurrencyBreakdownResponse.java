package com.shingirai.coindispense.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CurrencyBreakdownResponse implements Serializable {

    private List<RandCurrency> currencyList;

}
