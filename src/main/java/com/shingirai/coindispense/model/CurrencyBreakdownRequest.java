package com.shingirai.coindispense.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class CurrencyBreakdownRequest implements Serializable {

    private double amount;
    private String securityToken;

}
