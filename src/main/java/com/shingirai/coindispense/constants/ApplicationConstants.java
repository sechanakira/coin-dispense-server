package com.shingirai.coindispense.constants;

public class ApplicationConstants {

    private ApplicationConstants(){
    }

    public static final long SESSION_DURATION = 5;
    public static final int PORT = 8909;
}
