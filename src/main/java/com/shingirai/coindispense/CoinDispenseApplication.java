package com.shingirai.coindispense;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoinDispenseApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoinDispenseApplication.class, args);
	}

}
