package com.shingirai.coindispense.config;

import com.shingirai.coindispense.io.SocketServer;
import com.shingirai.coindispense.logic.BreakdownCalculator;
import com.shingirai.coindispense.persistence.repository.TokenRepository;
import com.shingirai.coindispense.persistence.repository.UserRepository;
import com.shingirai.coindispense.persistence.service.AuthenticationService;
import com.shingirai.coindispense.persistence.service.AuthenticationServiceImpl;
import com.shingirai.coindispense.persistence.service.TokenService;
import com.shingirai.coindispense.persistence.service.TokenServiceImpl;
import com.shingirai.coindispense.schedule.task.TokenScheduledTask;
import com.shingirai.coindispense.security.SecurityService;
import com.shingirai.coindispense.security.SecurityServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Bean
    public AuthenticationService authenticationService(UserRepository userRepository){
        return new AuthenticationServiceImpl(userRepository);
    }

    @Bean
    public SecurityService securityService(AuthenticationService authenticationService, TokenService tokenService){
        return new SecurityServiceImpl(authenticationService, tokenService);
    }

    @Bean
    public TokenService tokenService(TokenRepository tokenRepository){
        return new TokenServiceImpl(tokenRepository);
    }

    @Bean
    public TokenScheduledTask tokenScheduledTask(TokenService tokenService){
        return new TokenScheduledTask(tokenService);
    }

    @Bean
    public SocketServer socketServer(SecurityService securityService, BreakdownCalculator breakdownCalculator){
        SocketServer socketServer = new SocketServer();
        socketServer.setSecurityService(securityService);
        socketServer.setBreakdownCalculator(breakdownCalculator);
        return socketServer;
    }

}
