package com.shingirai.coindispense.schedule.task;

import com.shingirai.coindispense.persistence.service.TokenService;
import org.springframework.scheduling.annotation.Scheduled;

public class TokenScheduledTask {

    private TokenService tokenService;

    public TokenScheduledTask(TokenService tokenService){
        this.tokenService = tokenService;
    }

    @Scheduled(fixedDelay = 5 * 60 * 1000)
    public void removeExpiredTokens(){
        tokenService.removeExpiredTokens();
    }
}
