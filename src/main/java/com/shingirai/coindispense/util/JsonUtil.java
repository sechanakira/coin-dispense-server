package com.shingirai.coindispense.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.shingirai.coindispense.model.AuthenticationRequest;
import com.shingirai.coindispense.model.CurrencyBreakdownRequest;
import org.apache.log4j.Logger;

import java.io.IOException;

public class JsonUtil {

    private JsonUtil(){
    }

    private static final Logger logger = Logger.getLogger(JsonUtil.class);

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static <T> T readJSONObject(String json){
        try{
            if(json.contains("userName")){
                return objectMapper.readValue(json, (Class<T>) AuthenticationRequest.class);
            }
            else if(json.contains("token")){
                return objectMapper.readValue(json, (Class<T>) CurrencyBreakdownRequest.class);
            }
        }
        catch (IOException ioe){
            logger.info("Failed to read json object: ",ioe);
        }
        return null;
    }

    public static <T> String writeJSON(T t){
        try{
            return objectMapper.writeValueAsString(t);
        }
        catch (JsonProcessingException jpe){
            logger.info("Failed to write json object: ",jpe);
        }
        return "";
    }

}
