package com.shingirai.coindispense.persistence.service;

@FunctionalInterface
public interface AuthenticationService {

    boolean authenticate(String userName, String password);
}
