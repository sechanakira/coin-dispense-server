package com.shingirai.coindispense.persistence.service;

import com.shingirai.coindispense.persistence.model.Token;
import com.shingirai.coindispense.persistence.repository.TokenRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TokenServiceImpl implements TokenService {

    private TokenRepository tokenRepository;

    public TokenServiceImpl(TokenRepository tokenRepository){
        this.tokenRepository = tokenRepository;
    }

    @Override
    public String createLoginToken(String userName, String password) {
        String md5Hex = DigestUtils.md5Hex(userName + password);
        Token token = new Token();
        token.setToken(md5Hex);
        token.setSessionStartTime(LocalDateTime.now());
        token.setUserName(userName);
        tokenRepository.save(token);
        return md5Hex;
    }

    @Override
    public boolean validateToken(String userName, String token) {
        Token savedToken = tokenRepository.findByUserName(userName);
        if(null == savedToken){
            return false;
        }
        else{
            return savedToken.getToken().equals(token);
        }
    }

    @Override
    public boolean validateToken(String token) {
        return !tokenRepository.findByToken(token).isEmpty();
    }

    @Override
    public void removeExpiredTokens() {
        List<Token> expiredTokens = tokenRepository.findAll().stream()
                .filter(token-> Duration.between(token.getSessionStartTime(),
                        LocalDateTime.now()).getSeconds() >= 5 * 60)
                .collect(Collectors.toList());
        tokenRepository.deleteInBatch(expiredTokens);
    }

}
