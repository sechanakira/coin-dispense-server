package com.shingirai.coindispense.persistence.service;

import com.shingirai.coindispense.persistence.model.User;
import com.shingirai.coindispense.persistence.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private UserRepository userRepository;

    public AuthenticationServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public boolean authenticate(String userName, String password) {
        User user = userRepository.findByUserNameAndPassword(userName,password);
        return Objects.nonNull(user);
    }

}
