package com.shingirai.coindispense.persistence.service;

public interface TokenService {

    String createLoginToken(String userName, String password);
    boolean validateToken(String userName, String token);
    boolean validateToken(String token);
    void removeExpiredTokens();

}
