package com.shingirai.coindispense.persistence.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "token")
public class Token implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "token")
    private String token;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "session_start_time")
    private LocalDateTime sessionStartTime;

}
