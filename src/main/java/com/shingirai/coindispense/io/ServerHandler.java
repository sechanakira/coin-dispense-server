package com.shingirai.coindispense.io;

import com.shingirai.coindispense.logic.BreakdownCalculator;
import com.shingirai.coindispense.model.AuthenticationRequest;
import com.shingirai.coindispense.model.AuthenticationResponse;
import com.shingirai.coindispense.model.CurrencyBreakdownRequest;
import com.shingirai.coindispense.model.CurrencyBreakdownResponse;
import com.shingirai.coindispense.security.SecurityService;
import com.shingirai.coindispense.util.JsonUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class ServerHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = Logger.getLogger(JsonUtil.class);

    private SecurityService securityService;

    private BreakdownCalculator breakdownCalculator;

    public ServerHandler(SecurityService securityService, BreakdownCalculator breakdownCalculator){
        this.securityService = securityService;
        this.breakdownCalculator = breakdownCalculator;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        StringBuilder json = new StringBuilder("");
        ByteBuf in = (ByteBuf) msg;
        try {
            while (in.isReadable()) {
                json.append((char) in.readByte());
                System.out.flush();
            }
            Object request = JsonUtil.readJSONObject(json.toString());
            if(request instanceof AuthenticationRequest){
                AuthenticationRequest authenticationRequest = (AuthenticationRequest)request;
                AuthenticationResponse authenticationResponse = new AuthenticationResponse();
                authenticationResponse.setToken(securityService.login(authenticationRequest.getUserName(),authenticationRequest.getPassword()));
                String jsonResponse = JsonUtil.writeJSON(authenticationResponse);
                ctx.write(jsonResponse);
                ctx.flush();
            }
            else if(request instanceof CurrencyBreakdownRequest){
                CurrencyBreakdownRequest currencyBreakdownRequest = (CurrencyBreakdownRequest)request;
                if(!securityService.isValidToken(currencyBreakdownRequest.getSecurityToken())){
                    return;
                }
                CurrencyBreakdownResponse currencyBreakdownResponse = breakdownCalculator.breakDownCurrency(currencyBreakdownRequest);
                String jsonResponse = JsonUtil.writeJSON(currencyBreakdownResponse);
                ctx.write(jsonResponse);
                ctx.flush();
            }
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        logger.info("Exception: ", cause);
        ctx.close();
    }
}
