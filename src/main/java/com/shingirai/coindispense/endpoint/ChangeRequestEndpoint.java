package com.shingirai.coindispense.endpoint;

import com.shingirai.coindispense.logic.BreakdownCalculator;
import com.shingirai.coindispense.model.CurrencyBreakdownRequest;
import com.shingirai.coindispense.model.CurrencyBreakdownResponse;
import com.shingirai.coindispense.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/change")
public class ChangeRequestEndpoint {

    @Autowired
    private BreakdownCalculator breakdownCalculator;

    @Autowired
    private SecurityService securityService;

    @PostMapping(value = "/breakdown", consumes = "application/json", produces = "application/json")
    public CurrencyBreakdownResponse breakdown(@RequestBody CurrencyBreakdownRequest currencyBreakdownRequest){
        if (!securityService.isValidToken(currencyBreakdownRequest.getSecurityToken())){
            throw new RuntimeException("Unathenticated");
        }
        return breakdownCalculator.breakDownCurrency(currencyBreakdownRequest);
    }

}
