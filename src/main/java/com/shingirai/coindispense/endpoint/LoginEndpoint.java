package com.shingirai.coindispense.endpoint;

import com.shingirai.coindispense.model.AuthStatus;
import com.shingirai.coindispense.model.AuthenticationRequest;
import com.shingirai.coindispense.model.AuthenticationResponse;
import com.shingirai.coindispense.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginEndpoint {

    @Autowired
    private SecurityService securityService;

    @PostMapping(value = "/auth", consumes = "application/json", produces = "application/json")
    public AuthenticationResponse authenticate(@RequestBody AuthenticationRequest authenticationRequest){
        String authresponse = securityService.login(authenticationRequest.getUserName(),authenticationRequest.getPassword().toUpperCase());
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setToken(authresponse);
        if(authresponse.equals("AUTH_FAILED")){
            authenticationResponse.setAuthStatus(AuthStatus.AUTH_FAILED);
        }
        else {
            authenticationResponse.setAuthStatus(AuthStatus.AUTHENTICATED);
        }
        return authenticationResponse;
    }

}
