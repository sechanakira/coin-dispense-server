package com.shingirai.coindispense.security;

import com.shingirai.coindispense.persistence.service.AuthenticationService;
import com.shingirai.coindispense.persistence.service.TokenService;
import org.springframework.stereotype.Service;

@Service
public class SecurityServiceImpl implements SecurityService {

    private AuthenticationService authenticationService;

    private TokenService tokenService;

    public SecurityServiceImpl(AuthenticationService authenticationService, TokenService tokenService){
        this.authenticationService = authenticationService;
        this.tokenService = tokenService;
    }

    @Override
    public boolean isValidToken(String userName, String token) {
        return tokenService.validateToken(userName, token);
    }

    @Override
    public boolean isValidToken(String token) {
        return tokenService.validateToken(token);
    }

    @Override
    public String login(String userName, String password) {
        if (authenticationService.authenticate(userName, password)){
            return tokenService.createLoginToken(userName, password);
        }
        return "AUTH_FAILED";
    }

}
