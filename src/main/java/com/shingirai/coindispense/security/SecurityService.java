package com.shingirai.coindispense.security;

public interface SecurityService {

    boolean isValidToken(String userName, String token);
    boolean isValidToken(String token);
    String login(String userName, String password);

}
