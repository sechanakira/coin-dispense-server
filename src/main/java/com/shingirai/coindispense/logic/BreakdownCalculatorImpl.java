package com.shingirai.coindispense.logic;

import com.shingirai.coindispense.model.CurrencyBreakdownRequest;
import com.shingirai.coindispense.model.CurrencyBreakdownResponse;
import com.shingirai.coindispense.model.CurrencyType;
import com.shingirai.coindispense.model.RandCurrency;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class BreakdownCalculatorImpl implements BreakdownCalculator {

    @Override
    public CurrencyBreakdownResponse breakDownCurrency(CurrencyBreakdownRequest currencyBreakdownRequest) {
        CurrencyBreakdownResponse currencyBreakdownResponse = new CurrencyBreakdownResponse();
        List<RandCurrency> denominations = new ArrayList<>();
        String amount = String.valueOf(currencyBreakdownRequest.getAmount());
        String[] amountParts = amount.split("\\.");
        double integerPart = Double.parseDouble(amountParts[0]);
        double decimalPart = Double.parseDouble("0." + amountParts[1]);
        denominations.addAll(getDenominationsForIntegerPart(integerPart));
        denominations.addAll(getDenominationsForDecimalPart(decimalPart));
        currencyBreakdownResponse.setCurrencyList(denominations);
        return currencyBreakdownResponse;
    }

    private List<RandCurrency> getDenominationsForIntegerPart(double amt){
        double amount = amt;
        List<RandCurrency> denominations = new ArrayList<>();
        if(amount > 0){
            while (amount > 0){
                if(amount % 100 >= 0){
                    int numHundreds = (int)(amount - (amount % 100))/100;
                    IntStream.rangeClosed(1,numHundreds)
                            .forEach(i->denominations.add(RandCurrency.ONE_HUNDRED));
                    amount = amount % 100;
                }
                if(amount % 50 >= 0){
                    int numFifties = (int)(amount - (amount % 50))/50;
                    IntStream.rangeClosed(1,numFifties)
                            .forEach(i->denominations.add(RandCurrency.FIFTY));
                    amount = amount % 50;
                }
                if(amount % 20 >= 0){
                    int numTwenties = (int)(amount - (amount % 20))/20;
                    IntStream.rangeClosed(1,numTwenties)
                            .forEach(i->denominations.add(RandCurrency.TWENTY));
                    amount = amount % 20;
                }
                if(amount % 10 >= 0){
                    int numTens = (int)(amount - (amount % 10))/10;
                    IntStream.rangeClosed(1,numTens)
                            .forEach(i->denominations.add(RandCurrency.TEN));
                    amount = amount % 10;
                }
                if(amount % 5 >= 0){
                    int numFives = (int)(amount - (amount % 5))/5;
                    IntStream.rangeClosed(1,numFives)
                            .forEach(i->denominations.add(RandCurrency.FIVE));
                    amount = amount % 5;
                }
                if(amount % 2 >= 0){
                    int numTwos = (int)(amount - (amount % 2))/2;
                    IntStream.rangeClosed(1,numTwos)
                            .forEach(i->denominations.add(RandCurrency.TWO));
                    amount = amount % 2;
                }
                if(amount % 1 >= 0){
                    int numOnes = (int)(amount - (amount % 1))/1;
                    IntStream.rangeClosed(1,numOnes)
                            .forEach(i->denominations.add(RandCurrency.ONE));
                    amount = amount % 1;
                }
            }
        }
        return denominations;
    }

    private List<RandCurrency> getDenominationsForDecimalPart(double amt){
        double amount = amt;
        List<RandCurrency> coins = new ArrayList<>();
        if(amount > 0){
            while (amount > 0){
                if(amount >= 0.50){
                    coins.add(RandCurrency.FIFTY_CENTS);
                    amount -= 0.50;
                }
                if(amount >= 0.25){
                    coins.add(RandCurrency.TWENTY_FIVE_CENTS);
                    amount -= 0.25;
                }
                if(amount >= 0.10){
                    coins.add(RandCurrency.TEN_CENTS);
                    amount -= 0.10;
                }
                if(amount >= 0.05){
                    coins.add(RandCurrency.FIVE_CENTS);
                    amount -= 0.05;
                }
                if(amount < 0.05){
                    break;
                }
            }
        }
        return coins;
    }

    private List<RandCurrency> getAllDenominations(){
        return Arrays.asList(RandCurrency.values());
    }

    private List<RandCurrency> getDenominationsByType(CurrencyType currencyType){
        return Arrays.asList(RandCurrency.values()).stream()
                .filter(denomination->denomination.getCurrencyType().equals(currencyType))
                .collect(Collectors.toList());
    }

    private RandCurrency getByValue(double value){
        return getAllDenominations().stream()
                .filter(denomination->denomination.getValue() == value)
                .findFirst()
                .orElseThrow(()->new RuntimeException("Could not find a note with this value"));
    }

}
