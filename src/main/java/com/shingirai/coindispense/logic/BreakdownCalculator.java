package com.shingirai.coindispense.logic;

import com.shingirai.coindispense.model.CurrencyBreakdownRequest;
import com.shingirai.coindispense.model.CurrencyBreakdownResponse;

@FunctionalInterface
public interface BreakdownCalculator {

    CurrencyBreakdownResponse breakDownCurrency(CurrencyBreakdownRequest currencyBreakdownRequest);

}
